const browserSync = require('browser-sync').create();
const asciidoctor = require('@asciidoctor/core')();
const asciidoctorReveiljs = require('@asciidoctor/reveal.js');

asciidoctorReveiljs.register();

browserSync.watch('*.adoc', function(event, file) {
  if (event === 'change') {
    console.log(`${file} changed. start converting...`);
    asciidoctor.convertFile('ja.adoc', {safe: 'safe', backend: 'revealjs'});
    asciidoctor.convertFile('en.adoc', {safe: 'safe', backend: 'revealjs'});
    console.log(`reloading`);
    browserSync.reload('ja.html');
    browserSync.reload('en.html');
  }
});

browserSync.init({
  server: './',
  localOnly: true,
  open: 'local',
  ui: false,
}, function(err, _) {
  if (err) {
    console.error(err);
    return;
  }
});

