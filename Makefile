build:
	npx asciidoctor-revealjs ja.adoc
	npx asciidoctor-revealjs en.adoc

watch:
	npm run watch

setup:
	rm -rf reveal.js reveal.js.tar.gz
	mkdir reveal.js
	curl -L https://github.com/hakimel/reveal.js/archive/refs/tags/4.5.0.tar.gz > reveal.js.tar.gz
	tar -xvzf reveal.js.tar.gz -C reveal.js --strip-components 1
	rm -rf reveal.js.tar.gz

clean:
	rm *~
	rm *.html

